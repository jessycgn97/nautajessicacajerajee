package ec.edu.ups.servicios;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import ec.edu.ups.negocio.CajeraON;

@WebService
public class Servicios {
	
	@Inject 
	private CajeraON on;
	
	@WebMethod 
	public String recarga(String cedula, int numero, double valor) { 
		return on.realizarRecarga(cedula, numero, valor);
	} 

	
}


