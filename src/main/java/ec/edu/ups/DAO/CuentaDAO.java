package ec.edu.ups.DAO;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.modelo.Cliente;
import ec.edu.ups.modelo.Cuenta;

@Stateless
public class CuentaDAO {

	//Atributo de la clase
		@PersistenceContext(name = "NautaJessicaCajeraJEEPersistenceUnit") 
		private EntityManager em;
		
		/** 
		 * Metodo que permite registrar un cliente en la base de datos
		 * @param c Cliente que se va a registrar en la base
		 */
		public void insert(Cliente c) {
			em.persist(c);
		}
		
		public Cliente findByCedula(String cedula) throws Exception {
	        try {
	        	String jpql = "SELECT a FROM Cliente a WHERE a.cedula = :cedula";
	            Query q = em.createQuery(jpql, Cliente.class);
	            q.setParameter("cedula", cedula);

	            System.out.println("cedula: " + jpql);
	            System.out.println("ced: " + q);
	            //return em.find(Cliente.class, q);
	            return (Cliente) q.getSingleResult();
	        } catch (Exception e) {
	            throw new Exception("Error al buscar por cedula");
	        }
	    }
}
