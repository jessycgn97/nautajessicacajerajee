package ec.edu.ups.DAO;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ec.edu.ups.modelo.Cajera;

@Stateless
public class CajeraDAO {

	//Atributo de la clase
		@PersistenceContext(name = "NautaJessicaCajeraJEEPersistenceUnit") 
		private EntityManager em;
		
		/** 
		 * Metodo que permite registrar un cliente en la base de datos
		 * @param c Cajera que se va a registrar en la base
		 */
		public void insert(Cajera c) {
			em.persist(c);
		}
}
