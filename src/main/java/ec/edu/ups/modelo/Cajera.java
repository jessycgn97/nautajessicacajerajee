package ec.edu.ups.modelo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Cajera implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
//	private int numCuenta;
	private String cedula;
	private int numCelular;
	private double valor;
	
////	 @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	 @JoinColumn(name="cedula")
//	 private Cliente cliente;
	
	public Cajera() {
		super();
	}
	
	
	public Cajera(int id, String cedula, int numCelular, double valor) {
		super();
		this.id = id;
		this.cedula = cedula;
		this.numCelular = numCelular;
		this.valor = valor;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
//	public int getNumCuenta() {
//		return numCuenta;
//	}
//	public void setNumCuenta(int numCuenta) {
//		this.numCuenta = numCuenta;
//	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public int getNumCelular() {
		return numCelular;
	}
	public void setNumCelular(int numCelular) {
		this.numCelular = numCelular;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}


//	public Cliente getCliente() {
//		return cliente;
//	}
//
//	public void setCliente(Cliente cliente) {
//		this.cliente = cliente;
//	}

}
