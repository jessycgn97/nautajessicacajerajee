package ec.edu.ups.negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.DAO.CajeraDAO;
import ec.edu.ups.DAO.CuentaDAO;
import ec.edu.ups.modelo.Cajera;
import ec.edu.ups.modelo.Cliente;
import ec.edu.ups.modelo.Cuenta;

@Stateless
public class CuentaON {
	
	@Inject
	private CuentaDAO cuentadao;
	
	public void insert(Cliente cliente) {
		cuentadao.insert(cliente);
	}
	
	public Cliente obtenerCedula(String cedula) {
		try {
			return cuentadao.findByCedula(cedula);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
