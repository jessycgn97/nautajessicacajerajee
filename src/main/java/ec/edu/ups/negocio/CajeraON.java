package ec.edu.ups.negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.DAO.CajeraDAO;
import ec.edu.ups.modelo.Cajera;

@Stateless
public class CajeraON {
	
	@Inject
	private CajeraDAO cajeradao;
	
	public void insert(Cajera cajera) {
		cajeradao.insert(cajera);
	}

	public String realizarRecarga(String cedula, int numero, double valor) {
		try {
			Cajera cajera = new Cajera();
			cajera.setCedula(cedula);
			cajera.setNumCelular(numero);
			cajera.setValor(valor);
			cajeradao.insert(cajera);		
			
			return "Recarga realizada satisfactoriamente!!";
		} catch (Exception e) {
			return "No fue posible realizar la recarga";	
		}
	}
}
