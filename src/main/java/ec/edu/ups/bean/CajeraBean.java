package ec.edu.ups.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ec.edu.ups.modelo.Cajera;
import ec.edu.ups.modelo.Cliente;
import ec.edu.ups.negocio.CajeraON;
import ec.edu.ups.negocio.CuentaON;

@ManagedBean
@ViewScoped
public class CajeraBean {
	
	@Inject
	private CajeraON cajeraon;
	
	@Inject 
	private CuentaON cuentaon;
	
	private String cedula;
	
	private Cajera cajera;
	
	private Cliente cliente;
	
	@PostConstruct
	public void init() {
		cajera = new Cajera();
		cliente = new Cliente();
	}

	
	public CajeraON getCajeraon() {
		return cajeraon;
	}



	public void setCajeraon(CajeraON cajeraon) {
		this.cajeraon = cajeraon;
	}



	public Cajera getCajera() {
		return cajera;
	}



	public void setCajera(Cajera cajera) {
		this.cajera = cajera;
	}

	public CuentaON getCuentaon() {
		return cuentaon;
	}



	public void setCuentaon(CuentaON cuentaon) {
		this.cuentaon = cuentaon;
	}



	public String getCedula() {
		return cedula;
	}



	public void setCedula(String cedula) {
		this.cedula = cedula;
	}



	public Cliente getCliente() {
		return cliente;
	}



	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}



	public String guardarDatos() {
		
		System.out.println(this.toString());

		try {
			cajeraon.insert(cajera);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void buscar() {
		cliente = cuentaon.obtenerCedula(cedula);

	}

}
