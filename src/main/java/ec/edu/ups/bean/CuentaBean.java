package ec.edu.ups.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ec.edu.ups.modelo.Cajera;
import ec.edu.ups.modelo.Cliente;
import ec.edu.ups.modelo.Cuenta;
import ec.edu.ups.negocio.CajeraON;
import ec.edu.ups.negocio.CuentaON;

@ManagedBean
@ViewScoped
public class CuentaBean {
	
	@Inject
	private CuentaON cuentaon;
	
	private Cliente cliente;
	
	@PostConstruct
	public void init() {
		cliente = new Cliente();
	}
	

	public CuentaON getCuentaon() {
		return cuentaon;
	}

	public void setCuentaon(CuentaON cuentaon) {
		this.cuentaon = cuentaon;
	}

	

	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	public String guardarDatos() {
		
		System.out.println(this.toString());

		try {
			cuentaon.insert(cliente);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
